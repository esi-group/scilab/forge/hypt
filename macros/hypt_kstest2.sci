function [h,pval,ks] = hypt_kstest2 (x, y, alpha,tail)
// Two-sample Kolmogorov-Smirnov test
//Calling Sequence
//h = hypt_kstest (x)
//h = hypt_kstest (x, dist, distparam1,distparam2,...)
//h = hypt_kstest (x, dist, distparam1,distparam2,...,alt)
//[h,p] = hypt_kstest (...)
//[h,p, ksstat] = hypt_kstest (...)
//Parameters
//x : vector
//y : vector
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative mean, which is the default setting ('~=', '<>', 'both') and the one-sided Alternatives ('>','right') and ('<', 'left').
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
//Description
// Perform a 2-sample Kolmogorov-Smirnov test of the null hypothesis
// that the samples x and y come from the same (continuous)
// distribution.  I.e., if F and G are the CDFs corresponding to the
// x and y samples, respectively, then the null is that F ==
// G.
//
// Examples
// x =grand(20,1,"nor",0,1);
// y =grand(20,1,"nor",0,1);
// [h,p] = hypt_kstest2 (x,y)
// 
// [h,p] = hypt_kstest2 (x,y+20)
// 
// y =grand(20,1,"unf",2,4);
// [h,p] = hypt_kstest2 (x,y)
// 
// Authors
// H. Nahrstaedt - 2014
// Copyright (C) 1995-2011 Kurt Hornik


//
// This file is part of Octave.
//
// Octave is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// Octave is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Octave; see the file COPYING.  If not, see
// <http://www.gnu.org/licenses/>.

// -*- texinfo -*-
// @deftypefn {Function File} {[@var{pval}, @var{ks}, @var{d}] =} kolmogorov_smirnov_test_2 (@var{x}, @var{y}, @var{alt})
// Perform a 2-sample Kolmogorov-Smirnov test of the null hypothesis
// that the samples @var{x} and @var{y} come from the same (continuous)
// distribution.  I.e., if F and G are the CDFs corresponding to the
// @var{x} and @var{y} samples, respectively, then the null is that F ==
// G.
//
// With the optional argument string @var{alt}, the alternative of
// interest can be selected.  If @var{alt} is @code{"!="} or
// @code{"<>"}, the null is tested against the two-sided alternative F
// != G@.  In this case, the test statistic @var{ks} follows a two-sided
// Kolmogorov-Smirnov distribution.  If @var{alt} is @code{">"}, the
// one-sided alternative F > G is considered.  Similarly for @code{"<"},
// the one-sided alternative F < G is considered.  In this case, the
// test statistic @var{ks} has a one-sided Kolmogorov-Smirnov
// distribution.  The default is the two-sided case.
//
// The p-value of the test is returned in @var{pval}.
//
// The third returned value, @var{d}, is the test statistic, the maximum
// vertical distance between the two cumulative distribution functions.
//
// If no output argument is given, the p-value is displayed.
// @end deftypefn

// Author: KH <Kurt.Hornik@wu-wien.ac.at>
// Description: Two-sample Kolmogorov-Smirnov test
[nargout,nargin]=argn(0);


  [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_kstest2" , rhs , 2:4 );
  apifun_checklhs ( "hypt_kstest2" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_kstest2" , x , "x" , 1 , "constant" );
  apifun_checkrange("hypt_kstest2",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_kstest2",max(size(x)),"minimal size of x",1,2 );
    apifun_checktype ( "hypt_kstest2" , y , "y" , 2 , "constant" );
  apifun_checkrange("hypt_kstest2",length(size(y)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_kstest2",max(size(y)),"minimal size of x",1,2 );

  if (rhs < 3) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 4) | isempty(tail)
    tail = '~=';
  end




  n_x = length (x);
  n_y = length (y);
  n   = n_x * n_y / (n_x + n_y);
  x   = matrix (x, n_x, 1);
  y   = matrix (y, n_y, 1);
  [s, i] = mtlb_sort ([x; y]);
  count (find (i <= n_x)) = 1 / n_x;
  count (find (i > n_x)) = - 1 / n_y;

  z = cumsum (count);
  ds = diff (s);
  if (or (ds == 0))
    // There are some ties, so keep only those changes.
    warning ("cannot compute correct p-values with ties");
    elems = [find(ds)'; (n_x+n_y)'];
    z = z(elems);
  end

  if ( (tail== "~=") |  (tail == '!=') |  (tail== "<>")) | (tail == 'both'), 
    d    = max (abs (z));
    ks   = sqrt (n) * d;
    K   = ceil (sqrt (- log (%eps) / 2) / min (ks));
    k   = (1:K)';
    A   = exp (- 2 * k.^2 * ks.^2);
    odd = find (pmodulo (k, 2) == 1);
    A(odd,:) = -A(odd,:);
    pval =1-( 1 + 2 * sum (A));
  elseif ( (tail== ">")) | (tail == 'right'),
    d    = max (z);
    ks   = sqrt (n) * d;
    pval = exp (-2 * ks^2);
  elseif ( (alt== "<")) | (tail == 'left'),
    d    = min (z);
    ks   = -sqrt (n) * d;
    pval = exp (-2 * ks^2);
  else
    error (sprintf("kolmogorov_smirnov_test_2: option %s not recognized", alt));
  end

  if (nargout == 0)
    printf ("  pval: %g\n", pval);
  end
     h = pval < alpha;

endfunction
