// Copyright (C) 2013 - Holger Nahrstaedt
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h, pval, ci, stats] = hypt_vartest2 (x, y, alpha, tail,DIM)
//  Two-sample F-test for equal variances
//  
// Calling Sequence
// h = hypt_vartest2(x,y)
// h = hypt_vartest2(x,y,alpha)
// h = hypt_vartest2(x,y,alpha,tail)
// h = hypt_vartest2(x,y,alpha,tail,dim)
// [h,p] = hypt_vartest2(...)
// [h,p,ci] = hypt_vartest2(...)
// [h,p,ci,stats] = hypt_vartest2(...)'
// Parameters
// x: input vector or matrix
// y: input vector or matrix with the same size as x
// alpha: significance   level (default alpha = 0.05)
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative ('~=', '<>', 'both') and the one-sided Alternatives ('>','right') and ('<', 'left'). (default  tail = 'both')
// dim: if x is a matrix, dim defines the dimension along the test is performed (default is 'r' or 1)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
// ci: confidence internvals (depending on alpha)
// stats: structure with stats.fstat (value of test statistics), stats.df1 (degree of freedom from the numerator) , stats.df2 (degree of freedom from the numerator)
//
// Description
// Performs an F test to compare the variances of two samples from normal populations. If null hypothesis is true,
// then the variances if x abd y are not different. 
// 
//     hypt_vartest2 treads %nan as "Missing values" and ignores these. 
//
//     h=1 indicates a rejection of the Null-hypothesis at a significance 
//     level of alpha (default alpha = 0.05).	 
// 
//     With the optional argument string tail, the alternative of interest
//     can be selected.  If tail is '~=' or '<>' or 'both', the null hypothesis is tested
//     against the two-sided Alternative var (x) ~= var (y)'.  
//     
//     If tail    is '>' or 'right', the one-sided Alternative var (x) > var (y)' is used.
//     Similarly for '<' or 'left', the one-sided Alternative `var (x) < var (y)'  is used.  
//     The default is the two-sided case.
// Examples
//  batch1=[116 179 182 143 156 174]
//  batch2=[181 190 210 173 172 187]
//  [h, pval, ci, stats] = hypt_vartest2 (batch1,batch2)
//  // As h is false, there are not evidance that the variances of both batches is different
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt
// 



 [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_vartest2" , rhs , 2:5 );
  apifun_checklhs ( "hypt_vartest2" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_vartest2" , x , "x" , 1 , "constant" );
  apifun_checktype ( "hypt_vartest2" , y , "y" , 2 , "constant" );
  apifun_checkrange("hypt_vartest2",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_vartest2",max(size(x)),"minimal size of x",1,2 );

 

  if (rhs < 3) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 4) | isempty(tail)
    tail = '~=';
  end
  
  if (rhs < 5) | isempty(DIM)
    DIM = find(size(x)>1,1);
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
    
  elseif type(dim)~=10 then
    apifun_checktype ( "hypt_vartest2" , DIM , "dim" , 5 , "constant" );
    apifun_checkrange("hypt_vartest2",DIM,"dim",5,1,2 );
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
  end


  apifun_checktype ( "hypt_vartest2" , alpha , "alpha" , 3 , "constant" );
  apifun_checktype ( "hypt_vartest2" , tail , "tail" , 4 , "string" );
  apifun_checktype ( "hypt_vartest2" , dim , "dim" , 5 , "string" );

  szx = size(x); 
  szy = size(y);	
  szx(DIM)=1;
  szy(DIM)=1;
  
  if (or(szx-szy))
    error ('hypt_varest2: dimension of X and Y do not fit');
  end



  [x1,x2]=find(isnan(x));
  NX=size(x,dim)*ones(szx(1),szx(2));
  
  if (DIM==1) then
  for (i=1:size(x,2))
    NX(i)=NX(i)-length(find(x2==i));
  end;
  else
  for (i=1:size(x,1))
    NX(i)=NX(i)-length(find(x1==i));
  end;
  end;

  [y1,y2]=find(isnan(y));
  NY=size(y,dim)*ones(szy(1),szy(2));
  
  if (DIM==1) then
  for (i=1:size(y,2))
    NY(i)=NY(i)-length(find(y2==i));
  end;
  else
  for (i=1:size(y,1))
    NY(i)=NY(i)-length(find(y1==i));
  end;
  end;
   

  xmean=nanmean(x,dim);
  ymean=nanmean(y,dim);
  xvar=nanstdev(x,dim).^2;
  yvar=nanstdev(y,dim).^2;

  stats.df1 = NX - 1;
  stats.df2 = NY - 1;
  stats.fstat      = xvar ./ yvar;
 // cdf    = f_cdf (f, df_num, df_den);

  cdf = distfun_fcdf (stats.fstat, stats.df1, stats.df2);
  
  if ( (tail== '~=') |  (tail == '!=') |  (tail == '<>')) | (tail == 'both'),
    pval = 2 * min (cdf, 1 - cdf);
    ci = cat(DIM,stats.fstat.*distfun_finv(alpha/2,stats.df2,stats.df1),stats.fstat./distfun_finv(alpha/2,stats.df1,stats.df2));
  elseif  (tail ==  '>') | (tail == 'right'),
    pval = 1 - cdf;
    ci = cat(DIM,stats.fstat./distfun_finv(alpha/2,stats.df1,stats.df2),%inf);
  elseif  (tail == '<') | (tail == 'left'),
    pval = cdf;
    ci = cat(DIM,-%inf,stats.fstat./distfun_finv(alpha/2,stats.df1,stats.df2));
  else
    error (sprintf('hypt_ttest2: option %s not recognized', tail));
  end

  h = (pval < alpha);

endfunction
