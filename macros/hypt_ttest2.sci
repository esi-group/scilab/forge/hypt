// Copyright (C) 2013 - Holger Nahrstaedt
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h, pval, ci, stats] = hypt_ttest2 (x, y, alpha, tail, vartype, DIM)
// Two sample t-test and Welch test
// Calling Sequence
// h = hypt_ttest2(x,y)
// h = hypt_ttest2(x,y,alpha)
// h = hypt_ttest2(x,y,alpha,tail)
// h = hypt_ttest2(x,y,alpha,tail,vartype)
// h = hypt_ttest2(x,y,alpha,tail,vartype,dim)
// [h,p] = hypt_ttest2(...)
// [h ,p,ci] = hypt_ttest2(...)
// [h ,p,ci,stats] = hypt_ttest2(...)
// Parameters
// x: input vector or matrix
// y: input vector or matrix with the same size as x
// alpha: significance   level (default alpha = 0.05)
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative ('~=', '<>', 'both') and the one-sided alternatives ('>','right') and ('<', 'left'). (default  tail = 'both')
// vartype: supports 'equal' (default value) and 'unequal'. 
// dim: if x is a matrix, dim defines the dimension along the test is performed (default is 'r' or 1)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
// ci: confidence internvals (depending on alpha)
// stats: structure with stats.tstat (value of test statistics), stats.df (degree of freedom), stats.sd (standard deviation estimate), stats.mean (mean estimate of x or of  the difference)
//
// Description
//    ttest to compare  the means of two independent, random sampled groups under the assumtion that the samples
//    of both groups are normally distributed  with unknow but equal variances. This homogeneity of variances
//    can be verified using a fishers F-test (hypt_var).
//    
//    If both variances are not equal, then a welch test has to perfermed. This can be done with vartype="unequal".
//
//     hypt_ttest2 treads %nan as "Missing values" and ignores these. 
//
//     h=1 indicates a rejection of the Null-hypothesis at a significance 
//     level of alpha (default alpha = 0.05).	 
// 
//     With the optional argument string tail, the alternative of interest
//     can be selected.  If tail is '~=' or '<>' or 'both', the null hypothesis is tested
//     against the two-sided Alternative `mean (x) ~= mean (y)'.  
//     
//     If tail    is '>' or 'right', the one-sided Alternative `mean (x) > mean (y)' is used.
//     Similarly for '<' or 'left', the one-sided Alternative `mean (x) < mean (y)'  is used.  
//     The default is the two-sided case.
// 
// 
//     vartype supports 'equal', which means that a  two sample t-test is performed (default value) and 'unequal', 
//     which means that a  Welch test is performed. 
//  Examples
//  
//    WithDrug=[ 15,10,13,7,9,8,21,9,14,8];
//    Placebo=[15,14,12,8,14,7,16,10,15,12];
//    [h, pval, ci, stats] = hypt_ttest2 (WithDrug,Placebo)
//    //As h is false, the null hypothesis can not be rejected. 
//    //The Drug does not have any effect in comparison to the placebo.
//
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt
// 



  [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_ttest2" , rhs , 2:6 );
  apifun_checklhs ( "hypt_ttest2" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_ttest2" , x , "x" , 1 , "constant" );
  apifun_checkrange("hypt_ttest2",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_ttest2",max(size(x)),"minimal size of x",1,2 );


  if (rhs < 3) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 4) | isempty(tail)
    tail = '~=';
  end

  if (rhs < 5) | isempty(vartype)
    vartype = 'equal';
  end
  if ~(vartype=='equal' | vartype=='unequal')
    error ('hypt_ttest2: vartype not supported')
  end	
  if (rhs < 6) | isempty(DIM)
    DIM = find(size(x)>1,1);
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
    
  elseif type(dim)~=10 then
    apifun_checktype ( "hypt_ttest2" , DIM , "dim" , 6 , "constant" );
    apifun_checkrange("hypt_ttest2",DIM,"dim",6,1,2 );
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
  end

  apifun_checktype ( "hypt_ttest2" , y , "y" , 2 , "constant" );
  apifun_checktype ( "hypt_ttest2" , alpha , "alpha" , 3 , "constant" );
  apifun_checktype ( "hypt_ttest2" , tail , "tail" , 4 , "string" );
  apifun_checktype ( "hypt_ttest2" , vartype , "vartype" , 5 , "string" );
  apifun_checktype ( "hypt_ttest2" , dim , "dim" , 6 , "string" );

  szx = size(x); 
  szy = size(y);	
  szx(DIM)=1;
  szy(DIM)=1;
  
  if (or(szx-szy))
    error ('hypt_ttest: dimension of X and Y do not fit');
  end

   SX = nansum(x,dim);
   SY = nansum(y,dim);

  [x1,x2]=find(isnan(x));
  NX=size(x,dim)*ones(szx(1),szx(2));
  
  if (DIM==1) then
  for (i=1:size(x,2))
    NX(i)=NX(i)-length(find(x2==i));
  end;
  else
  for (i=1:size(x,1))
    NX(i)=NX(i)-length(find(x1==i));
  end;
  end;

  [y1,y2]=find(isnan(y));
  NY=size(y,dim)*ones(szy(1),szy(2));
  
  if (DIM==1) then
  for (i=1:size(y,2))
    NY(i)=NY(i)-length(find(y2==i));
  end;
  else
  for (i=1:size(y,1))
    NY(i)=NY(i)-length(find(y1==i));
  end;
  end;
   
  MX = SX ./ NX;
  MY = SY ./ NY;
  xmean=nanmean(x,dim);
  ymean=nanmean(y,dim);
  xvar=nanstdev(x,dim).^2;
  yvar=nanstdev(y,dim).^2;

  if vartype=='equal' then
    stats.df = NX + NY - 2;
    sPooled = sqrt(((NX-1) .* xvar + (NY-1) .* yvar) ./ stats.df);
    se = sPooled .* sqrt(1 ./NX + 1 ./NY);


    if or(size(x)==0) | or(size(y)==0)
	  v = %nan;
    else	
	  v = nansum((x-repmat(MX,size(x)./size(MX))).^2,dim) + nansum((y-repmat(MY,size(y)./size(MY))).^2,dim);
    end; 	
    stats.sd    = sqrt(v./stats.df);
    stats.tstat = (MX - MY) .* sqrt ((NX .* NY .* stats.df) ./ (v .* (NX + NY)));
  else
    s2xbar = xvar ./ NX;
    s2ybar = yvar ./ NY;
    stats.df = (s2xbar + s2ybar) .^2 ./ (s2xbar.^2 ./ (NX-1) + s2ybar.^2 ./ (NY-1));
    stats.sd= sqrt(cat(DIM, xvar, yvar))
    se = sqrt(s2xbar + s2ybar);
    stats.tstat = (MX-MY) ./ se;

  end
  stats.xmean=xmean;
  stats.ymean=ymean;

  cdf = distfun_tcdf (stats.tstat, stats.df);

  if ( (tail== '~=') |  (tail == '!=') |  (tail == '<>')) | (tail == 'both'),
    pval = 2 * min (cdf, 1 - cdf);
     crit = distfun_tinv((1 - alpha / 2), stats.df) .* se;
    ci = cat(DIM,(xmean-ymean)-crit,(xmean-ymean)+crit);
  elseif  (tail ==  '>') | (tail == 'right'),
    pval = 1 - cdf;
     crit = distfun_tinv((1 - alpha ), stats.df) .* se;
    ci = cat(DIM,(xmean-ymean)-crit,%inf);
  elseif  (tail == '<') | (tail == 'left'),
    pval = cdf;
    crit = distfun_tinv((1 - alpha), stats.df) .* se;
    ci = cat(DIM,-%inf,(xmean-ymean)+crit);
  else
    error (sprintf('hypt_ttest2: option %s not recognized', tail));
  end

  h = (pval < alpha);	


endfunction
