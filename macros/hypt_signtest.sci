// Copyright (C) 2013 - Holger Nahrstaedt
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h,pval, stats] = hypt_signtest (x, y, alpha, tail,DIM)
//Sign test
// Calling Sequence
// h =  hypt_signtest (x, y)
// h =  hypt_signtest (x, y,alpha)
// h =  hypt_signtest (x, y,alpha,tail)
// [h,p] = hypt_signtest(...)
// [h,p,stats] = hypt_signtest(...)
// Parameters
// x: input vector or matrix
// y: input vector or matrix with the same size as x
// alpha: significance   level (default alpha = 0.05)
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative  ('~=', '<>', 'both') and the one-sided alternatives ('>','right') and ('<', 'left'). (default  tail = 'both')
// dim: if x is a matrix, dim defines the dimension along the test is performed (default is 'r' or 1)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
// stats: structure with stats.zval (value of z statistics), stats.sign (value of sign statistics)
// Description
// 
// For two matched-pair samples x and y, perform a sign test
// of the null hypothesis PROB (x > y) == PROB (x < y) == 1/2.  
// Under the null, the test statistic  roughly
// follows a binomial distribution with parameters n = sum (x~= y) and p = 1/2.
//
//     hypt_signtest treads %nan as "Missing values" and ignores these. 
//
//     h=1 indicates a rejection of the Null-hypothesis at a significance 
//     level of alpha (default alpha = 0.05).	 
// 
//     With the optional argument string tail, the alternative of interest
//     can be selected.  If tail is '~=' or '<>' or 'both', the null hypothesis is tested
//     against the two-sided Alternative `mean (x) ~= mean (y)'.  
//     
//     If tail    is '>' or 'right', the one-sided Alternative `mean (x) > mean (y)' is used.
//     Similarly for '<' or 'left', the one-sided Alternative `mean (x) < mean (y)'  is used.  
//     The default is the two-sided case.
//
// Examples
// before=[5 3 4 2 1 6 7 3 2 3 5 1 4 4 3];
// after =[6 2 4 4 3 6 7 5 3 5 5 3 4 5 2];
// [h,p] = hypt_signtest(before,after,0.05,"<")
// 
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt
// 




 [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_signtest" , rhs , 2:5 );
  apifun_checklhs ( "hypt_signtest" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_signtest" , x , "x" , 1 , "constant" );
  apifun_checktype ( "hypt_signtest" , y , "y" , 2 , "constant" );
  apifun_checkrange("hypt_signtest",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_signtest",max(size(x)),"minimal size of x",1,2 );

 

  if (rhs < 3) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 4) | isempty(tail)
    tail = '~=';
  end
  
  if (rhs < 5) | isempty(DIM)
    DIM = find(size(x)>1,1);
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
    
  elseif type(dim)~=10 then
    apifun_checktype ( "hypt_signtest" , DIM , "dim" , 5 , "constant" );
    apifun_checkrange("hypt_signtest",DIM,"dim",5,1,2 );
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
  end


  apifun_checktype ( "hypt_signtest" , alpha , "alpha" , 3 , "constant" );
  apifun_checktype ( "hypt_signtest" , tail , "tail" , 4 , "string" );
  apifun_checktype ( "hypt_signtest" , dim , "dim" , 5 , "string" );

  szx = size(x); 
  szy = size(y);	
  szx(DIM)=1;
  szy(DIM)=1;
  
  if (or(szx-szy))
    error ('hypt_signtest: dimension of X and Y do not fit');
  end

  [x1,x2]=find(isnan(x));
  N=size(x,dim)*ones(szx(1),szx(2));
  
//   if (DIM==1) then
//   for (i=1:size(x,2))
//     N(i)=N(i)-length(find(x2==i));
//   end;
//   else
//   for (i=1:size(x,1))
//     N(i)=N(i)-length(find(x1==i));
//   end;
//   end;
  

  stats.sign = nansum(x-y < 0,dim);
   


   if ( (tail== '~=') |  (tail == '!=') |  (tail == '<>')) | (tail == 'both'),
      plow = distfun_binocdf (stats.sign - 1, N, 1 / 2,%f);
      pup  =  distfun_binocdf (stats.sign, N, 1/2);
      pval = min(2 * plow, 2 * pup, 1);
  elseif  (tail ==  '>') | (tail == 'right'),
    pval = distfun_binocdf (stats.sign - 1, N, 1 / 2,%f);
      
  elseif  (tail == '<') | (tail == 'left'),
    pval = distfun_binocdf (stats.sign, N, 1/2);
  else
    error (sprintf("hypt_signtest: option %s not recognized", tail));
  end

  //h = pval <= alpha;	
  h=(pval < alpha);


endfunction
