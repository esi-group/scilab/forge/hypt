// Copyright (C) 2013 - Holger Nahrstaedt
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h,pval, stats] = hypt_signrank (x, y, alpha,tail,DIM)
//Wilcoxon signed-rank test
//Calling Sequence
//h=hypt_signrank (x, y)
//h=hypt_signrank (x, y, alpha)
//h=hypt_signrank (x, y, alpha,tail)
//[h,pval] = hypt_signrank(..)
//[h,pval, stats] = hypt_signrank(..)
//Parameters
// x: input vector or matrix
// y: input vector or matrix with the same size as x
// alpha: significance   level (default alpha = 0.05)
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative('~=', '<>', 'both') and the one-sided alternatives ('>','right') and ('<', 'left'). (default  tail = 'both')
// dim: if x is a matrix, dim defines the dimension along the test is performed (default is 'r' or 1)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
// stats: structure with stats.zval (value of z statistics), stats.sign (value of sign statistics)
// Description
// For two matched-pair sample vectors x and y, perform a
// Wilcoxon signed-rank test of the null hypothesis PROB (x > y) == 1/2.  Under the null, the test statistic z
// approximately follows a standard normal distribution when n > 5.
//
// This function assumes a normal distribution for z
// and thus is invalid for n < 5.
//
//     hypt_signrank treads %nan as "Missing values" and ignores these. 
//
//     h=%t indicates a rejection of the Null-hypothesis at a significance 
//     level of alpha (default alpha = 0.05).	 
// 
//     With the optional argument string tail, the alternative of interest
//     can be selected.  If tail is '~=' or '<>' or 'both', the null hypothesis is tested
//     against the two-sided Alternative `mean (x) ~= mean (y)'.  
//     
//     If tail    is '>' or 'right', the one-sided Alternative `mean (x) > mean (y)' is used.
//     Similarly for '<' or 'left', the one-sided Alternative `mean (x) < mean (y)'  is used.  
//     The default is the two-sided case.
// 
// Examples
// 
// x=[125,115,130,140,140,115,140,125,140,135];
// y=[110,122,125,120,140,124,123,137,135,145];
// 
// [h,pval, stats] = hypt_signrank(x,y);
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt
// 


 [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_signrank" , rhs , 2:5 );
  apifun_checklhs ( "hypt_signrank" , lhs , 0:4 );
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_signrank" , x , "x" , 1 , "constant" );
  apifun_checktype ( "hypt_signrank" , y , "y" , 2 , "constant" );
  apifun_checkrange("hypt_signrank",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_signrank",max(size(x)),"minimal size of x",1,2 );

 

  if (rhs < 3) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 4) | isempty(tail)
    tail = '~=';
  end
  
  if (rhs < 5) | isempty(DIM)
    DIM = find(size(x)>1,1);
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
    
  elseif type(dim)~=10 then
    apifun_checktype ( "hypt_signrank" , DIM , "dim" , 5 , "constant" );
    apifun_checkrange("hypt_signrank",DIM,"dim",5,1,2 );
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
  end


  apifun_checktype ( "hypt_signrank" , alpha , "alpha" , 3 , "constant" );
  apifun_checktype ( "hypt_signrank" , tail , "tail" , 4 , "string" );
  apifun_checktype ( "hypt_signrank" , dim , "dim" , 5 , "string" );

  szx = size(x); 
  szy = size(y);	
  //szx(DIM)=1;
  //szy(DIM)=1;

  
  n = length (x);
  x = matrix (x, 1, n);
  y = matrix (y, 1, n);
  d = x - y;
  d(isnan(d)) = [];

  d = d (find (d ~= 0));

  n = length(d);

  if (n==0) then
    pval=1;
    h=%f;
    stats=[];
    return;
  end;

    if n<=5
        error("implementation requires more than 5 different pairs!");
    end

    X = abs (d);
    X = X(:);

  [sx, rowidx] = mtlb_sort(X(:));
  ranks = [1:n]';
  tieadj = 0;
  ties = (sx(1:n-1) == sx(2:n));
  tieloc = [find(ties)'; n+2];
  maxTies = length(tieloc);

  tiecount = 1;
  while (tiecount < maxTies)
      tiestart = tieloc(tiecount);
      ntied = 2;
      while(tieloc(tiecount+1) == tieloc(tiecount)+1)
	  tiecount = tiecount+1;
	  ntied = ntied+1;
      end
      tieadj = tieadj + ntied*(ntied-1)*(ntied+1)/2; 
      ranks(tiestart:tiestart+ntied-1) =  sum(ranks(tiestart:tiestart+ntied-1)) / ntied;
      tiecount = tiecount + 1;
  end


  r(rowidx) = ranks;
  r = matrix(r,size(X));

    //w = sum(r.*sign(d)');
    neg = find(d<0);
    w = sum(r(neg));
    w = min(w, n*(n+1)/2-w);
    stats.signrank = sum(r.*sign(d)')+w;

     
    z = ((w - n * (n + 1) / 4) / sqrt ((n * (n + 1) * (2 * n + 1) - tieadj) / 24)); 
    stats.z=z;

    
    cdf = distfun_normcdf(z,0,1)

      if ( (tail== '~=') |  (tail == '!=') |  (tail == '<>')) | (tail == 'both'),
	pval = 2 * min (cdf, 1 - cdf);
      elseif ( (tail== ">"))| (tail == 'right'),
	pval = 1 - cdf;
      elseif ( (tail=="<")) | (tail == 'left'),
	pval = cdf;
      else
	error (sprintf("hypt_signrank: option %s not recognized", tail));
      end


    h = pval < alpha;	


endfunction






