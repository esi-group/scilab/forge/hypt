// Copyright (C) 2013 - Holger Nahrstaedt
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h, pval, ci, stats] = hypt_ttest (x, m, alpha, tail,DIM)
//  one-sample and paired t-test
//  
// Calling Sequence
// h = hypt_ttest(x)	
// h = hypt_ttest(x,m)
// h = hypt_ttest(x,y)
// h = hypt_ttest(x,y,alpha)
// h = hypt_ttest(x,y,alpha,tail)
// h = hypt_ttest(x,y,alpha,tail,dim)
// [h,p] = hypt_ttest(...)
// [h,p,ci] = hypt_ttest(...)
// [h,p,ci,stats] = hypt_ttest(...)
// 
// Parameters
// x: input vector or matrix
// m: mean value for testing (default m = 0)
// y: input vector or matrix with the same size as x
// alpha: significance   level (default alpha = 0.05)
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative ('~=', '<>', 'both') and the one-sided alternatives ('>','right') and ('<', 'left'). (default  tail = 'both')
// dim: if x is a matrix, dim defines the dimension along the test is performed (default is 'r' or 1)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
// ci: confidence internvals (depending on alpha)
// stats: structure with stats.tstat (value of test statistics), stats.df (degree of freedom), stats.sd (standard deviation estimate), stats.mean (mean estimate of x or of  the difference)
// 
// Description
//	
//     For a given vector x, which contains samples from a normal distribution with unknown mean and
//     variance, hypt_ttest(x,m) performs a one-sample t-test of the null hypothesis H0 'mean (x) == m' against 
//     the alternative hypothesis H1 'mean(x) ~= m'. The default value of m is zero.
//         
//     Under the assumtion that the hypothesis being tested is true, the test statistic t follows a Student
//     distribution with `df = length (x) - 1' degrees of freedom.
//
//     h=%f indicates that the null-hypothesis  at a significance 
//     level of alpha (default alpha = 0.05) could not be rejected.	
//
//     h=%t indicates a rejection of the null-hypothesis  at a significance 
//     level of alpha (default alpha = 0.05).
//     
//     A paired t-test is performed, if a second sample y  is given. y(i) is a independend measurement on the same object / subject as x(i). x and y must have the same size. 
//    hypt_ttest(x,y) performs a paried t-test of the null hypothesis H0, which is  'mean (x) == mean (y)', against the null hypothesis H1, which is defined as 'mean(x) ~= mean(y)'.
// 
//     With the optional argument string tail, the alternative of interest
//     can be selected.  If tail is '~=' or '<>' or 'both', the null hypothesis is tested
//     against the two-sided Alternative `mean (x) ~= mean (y)' or `mean (x) ~= m'.  
//     
//     If tail    is '>' or 'right', the one-sided Alternative `mean (x) > mean (y)' or `mean (x) > m' is used.
//     Similarly for '<' or 'left', the one-sided Alternative `mean (x) < mean (y)' or `mean (x) < m' is used.  The default is the two-sided case.
// 
//
// Examples
// // We have a treatment and a measurement on 10 subjects before the treatment 
// // and after the treatment. Now we testing if the null hypothesis H0 
// // ('mean (x-y) == 0') against the alternative hypothesis H1 ('mean(x-y) ~= 0').
// // If the null hypothesis could not be rejected, the treatment would not have 
// // any effect or the number is to small.
// 
// before=[223, 259, 248, 220, 287, 191, 229, 270, 245, 201];
// after =[220, 244, 243, 211, 299, 170, 210, 276, 242, 189];
// [h,p]=hypt_ttest(before,after);
// disp(h);
// // h=%t indicates that the null hypothesis could be rejected. The treatment has a 
// // effect.
// 
// //We are testing now if the treatment reduces the value.
// [h,p]=hypt_ttest(before,after,0.05,'<'); 
// // h = %t indicates that the alternative hypothesis ('mean (x-y) < 0') should be 
// // chosen.  The treatment reduces the value with a significance level 1-p.
//  
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt
// 



  [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_ttest" , rhs , 1:5 );
  apifun_checklhs ( "hypt_ttest" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_ttest" , x , "x" , 1 , "constant" );
  apifun_checkrange("hypt_ttest",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_ttest",max(size(x)),"minimal size of x",1,2 );

 
  if (rhs < 2) | isempty(m)
    m = 0;
  end

  if (rhs < 3) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 4) | isempty(tail)
    tail = '~=';
  end
  
  if (rhs < 5) | isempty(DIM)
    DIM = find(size(x)>1,1);
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
    
  elseif type(dim)~=10 then
    apifun_checktype ( "hypt_ttest" , DIM , "dim" , 5 , "constant" );
    apifun_checkrange("hypt_ttest",DIM,"dim",5,1,2 );
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
  end


  apifun_checktype ( "hypt_ttest" , m , "m" , 2 , "constant" );
  apifun_checktype ( "hypt_ttest" , alpha , "alpha" , 3 , "constant" );
  apifun_checktype ( "hypt_ttest" , tail , "tail" , 4 , "string" );
  apifun_checktype ( "hypt_ttest" , dim , "dim" , 5 , "string" );

  szx = size(x); 
  szm = size(m);	
  //check nan

  if max(szm)==1
  	; //m is a skalar
  	
  elseif szx == szm
        y = m;
  	x = x-y;
  	szm(DIM)=1;
  	m = zeros(szm(1),szm(2));
  else
    error ('hypt_ttest: dimension of X and Y do not fit');
  end 	  
  xmean=nanmean(x,dim);
  S = nansum(x,dim);

  [x1,x2]=find(isnan(x));
  N=size(x,dim)*ones(szm(1),szm(2));
  
  if (DIM==1) then
  for (i=1:size(x,2))
    N(i)=N(i)-length(find(x2==i));
  end;
  else
  for (i=1:size(x,1))
    N(i)=N(i)-length(find(x1==i));
  end;
  end;
  
  
  stats.mean = nanmean(x,dim);
  stats.df = N - 1;
  stats.sd = nanstdev(x,dim);
  stats.tstat = sqrt (N) .* (S./N - m) ./ stats.sd;
  //cdf = tcdf (stats.tstat, stats.df);
  cdf = distfun_tcdf (stats.tstat, stats.df);
  //cdf = cdft("PQ",stats.tstat,stats.df);

  if ( (tail == '~=') |  (tail == '!=') |  (tail ==  '<>')) | (tail == 'both'),
    pval = 2 * min (cdf, 1 - cdf);
    crit = distfun_tinv((1 - alpha / 2), stats.df) .* (stats.sd ./sqrt(N));
    ci = cat(DIM, xmean - crit, xmean  + crit);
  elseif  (tail == '>') | (tail == 'right'),
    crit = distfun_tinv((1 - alpha ), stats.df) .* (stats.sd ./sqrt(N));
    ci = cat(DIM, xmean - crit, %inf);
    pval = 1 - cdf;
  elseif  (tail == '<') | (tail == 'left'),
    crit = distfun_tinv((1 - alpha ), stats.df) .* (stats.sd ./sqrt(N));
    ci = cat(DIM, -%inf, xmean  + crit);
    pval = cdf;
  else
    error (sprintf('hypt_ttest: option %s not recognized', tail));
  end

  h=(pval < alpha);	

endfunction
