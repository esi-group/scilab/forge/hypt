function [h,pval,ks] = hypt_kstest (x, dist, varargin)
// One-sample Kolmogorov-Smirnov test
//Calling Sequence
//h = hypt_kstest (x)
//h = hypt_kstest (x, dist, distparam1,distparam2,...)
//h = hypt_kstest (x, dist, distparam1,distparam2,...,alt)
//[h,p] = hypt_kstest (...)
//[h,p, ksstat] = hypt_kstest (...)
//Parameters
//x : vector
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative mean, which is the default setting ('~=', '<>', 'both') and the one-sided Alternatives ('>','right') and ('<', 'left').
//dist: string, can be any string for which a function distfun_...cdf that calculates the CDF of distribution dists exists.
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
//Description
// Perform a Kolmogorov-Smirnov test of the null hypothesis that the
// sample x comes from the (continuous) distribution dist.  I.e.,
// if F and G are the CDFs corresponding to the sample and dist,
// respectively, then the null is that F == G.
//
// The optional arguments distparam1,distparam2 contains  parameters of
// dist.  For example, to test whether a sample x comes from
// a uniform distribution on [2,4], use
// [h,pval] = hypt_kstest (x,"unif",2,4);
// Examples
// x =grand(20,1,"nor",0,1);
// [h,p] = hypt_kstest (x)
// 
// x =grand(20,1,"unf",2,4);
// [h,p] = hypt_kstest (x)
// [h,p] = hypt_kstest (x,"unif",2,4)
// Authors
// H. Nahrstaedt - 2014
// Copyright (C) 1995-2011 Kurt Hornik


//
// This file is part of Octave.
//
// Octave is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// Octave is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Octave; see the file COPYING.  If not, see
// <http://www.gnu.org/licenses/>.

// -*- texinfo -*-
// @deftypefn {Function File} {[@var{pval}, @var{ks}] =} kolmogorov_smirnov_test (@var{x}, @var{dist}, @var{params}, @var{alt})
// Perform a Kolmogorov-Smirnov test of the null hypothesis that the
// sample @var{x} comes from the (continuous) distribution dist.  I.e.,
// if F and G are the CDFs corresponding to the sample and dist,
// respectively, then the null is that F == G.
//
// The optional argument @var{params} contains a list of parameters of
// @var{dist}.  For example, to test whether a sample @var{x} comes from
// a uniform distribution on [2,4], use
//
// @example
// kolmogorov_smirnov_test(x, "unif", 2, 4)
// @end example
//
// @noindent
// @var{dist} can be any string for which a function @var{dist_cdf}
// that calculates the CDF of distribution @var{dist} exists.
//
// With the optional argument string @var{alt}, the alternative of
// interest can be selected.  If @var{alt} is @code{"!="} or
// @code{"<>"}, the null is tested against the two-sided alternative F
// != G@.  In this case, the test statistic @var{ks} follows a two-sided
// Kolmogorov-Smirnov distribution.  If @var{alt} is @code{">"}, the
// one-sided alternative F > G is considered.  Similarly for @code{"<"},
// the one-sided alternative F > G is considered.  In this case, the
// test statistic @var{ks} has a one-sided Kolmogorov-Smirnov
// distribution.  The default is the two-sided case.
//
// The p-value of the test is returned in @var{pval}.
//
// If no output argument is given, the p-value is displayed.
// @end deftypefn

// Author: KH <Kurt.Hornik@wu-wien.ac.at>
// Description: One-sample Kolmogorov-Smirnov test

[nargout,nargin]=argn(0);
  [lhs,rhs]=argn();
  //apifun_checkrhs ( "hypt_kstest" , rhs , 1:3 );
  apifun_checklhs ( "hypt_kstest" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_kstest" , x , "x" , 1 , "constant" );
  apifun_checkrange("hypt_kstest",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_kstest",max(size(x)),"minimal size of x",1,2 );



  if (rhs < 2) | isempty(dist)
    dist = "norm";
  end
    apifun_checktype ( "hypt_kstest" , dist , "dist" , 2 , "string" );
alpha = .05;
   if (rhs < 1)
    error("wrong number of parameters") ;
   end

//   if (~ isvector (x))
//     error ("kolmogorov_smirnov_test: X must be a vector");
//   end

  n = length (x);
  s = mtlb_sort (x);
  try
    f = eval (sprintf ("distfun_%scdf", dist));
  catch
    try
      f = eval (sprintf ("%scdf", dist));
    catch
      error (sprintf("kolmogorov_smirnov_test: no distfun_%scdf or %scdf function found",             dist, dist));
    end
  end

  tail  = "~=";

  //args(1) = s;
  if (rhs>2) then
    nvargs = length (varargin);

    if (nvargs > 0)
      if (type (varargin($))==10)
	tail = varargin($);
	nvargs=nvargs-1;
      end
    end
  else
    nvargs=0;
  end;
  
  if (nvargs > 0)
    if (nvargs==1) then
     z = matrix (f(s,varargin(1)), 1, n);
    elseif (nvargs==2) then
    z = matrix (f(s,varargin(1),varargin(2)), 1, n);
    elseif (nvargs==3) then
    z = matrix (f(s,varargin(1),varargin(2),varargin(3)), 1, n);
    elseif (nvargs==4) then
    z = matrix (f(s,varargin(1),varargin(2),varargin(3),varargin(4)), 1, n);
    elseif (nvargs==5) then
    z = matrix (f(s,varargin(1),varargin(2),varargin(3),varargin(4),varargin(5)), 1, n);
    else
    error("wrong number of parameters") ;
    end;
  else
    z = matrix (f(s,0,1), 1, n);
  end;

  if ( (tail== "~=") |  (tail == '!=') |  (tail== "<>")) | (tail == 'both'), 
    ks   = sqrt (n) * max ([abs(z - (0:(n-1))/n); abs(z - (1:n)/n)]);
    
    K   = ceil (sqrt (- log (%eps) / 2) / min (ks));
    k   = (1:K)';
    A   = exp (- 2 * k.^2 * ks.^2);
    odd = find (pmodulo (k, 2) == 1);
    A(odd,:) = -A(odd,:);
    pval =1-( 1 + 2 * sum (A));

    
  elseif ( (tail== ">")) | (tail == 'right'),
    ks   = sqrt (n) * max (max ([z - (0:(n-1))/n; z - (1:n)/n]));
    pval = exp (- 2 * ks^2);
  elseif ( (alt== "<")) | (tail == 'left'),
    ks   = - sqrt (n) * min (min ([z - (0:(n-1))/n; z - (1:n)/n]));
    pval = exp (- 2 * ks^2);
  else
    error ("hypt_kstest: alternative %s not recognized", alt);
  end

  if (nargout == 0)
    printf ("pval: %g\n", pval);
  end
  
   h = pval < alpha;

endfunction

// %!error <Invalid call to kolmogorov_smirnov_test>
// %!  kolmogorov_smirnov_test (1);
// %!error <kolmogorov_smirnov_test: X must be a vector>
// %!  kolmogorov_smirnov_test ({}, "unif", 2, 4);
// %!error <kolmogorov_smirnov_test: no not_a_distcdf or not_a_dist_cdf function found>
// %!  kolmogorov_smirnov_test (1, "not_a_dist");
// %!error <kolmogorov_smirnov_test: alternative bla not recognized>
// %!  kolmogorov_smirnov_test (1, "unif", 2, 4, "bla");
// %!test # for recognition of unifcdf function
// %!  assert (kolmogorov_smirnov_test (0:100, "unif", 0, 100), 1.0, eps);
// %!test # for recognition of logistic_cdf function
// %!  assert (kolmogorov_smirnov_test (0:100, "logistic"), 0);
// %!test # F < G
// %!  assert (kolmogorov_smirnov_test (50:100, "unif", 0, 50, "<"));
// 
// 
// function cdf = kolmogorov_smirnov_cdf (x, tol)
// 
//   if (nargin < 1 || nargin > 2)
//     print_usage ();
//   endif
// 
//   if (nargin == 1)
//     if (isa (x, "single"))
//       tol = eps ("single");
//     else
//       tol = eps;
//     endif
//   else
//     if (! isscalar (tol) || ! (tol > 0))
//       error ("kolmogorov_smirnov_cdf: tol has to be a positive scalar");
//     endif
//   endif
// 
//   n = numel (x);
//   if (n == 0)
//     error ("kolmogorov_smirnov_cdf: x must not be empty");
//   endif
// 
//   cdf = zeros (size (x));
// 
//   ind = find (x > 0);
//   if (length (ind) > 0)
//     if (size(ind,2) < size(ind,1))
//       y = x(ind.');
//     else
//       y   = x(ind);
//     endif
//     K   = ceil (sqrt (- log (tol) / 2) / min (y));
//     k   = (1:K)';
//     A   = exp (- 2 * k.^2 * y.^2);
//     odd = find (rem (k, 2) == 1);
//     A(odd,:) = -A(odd,:);
//     cdf(ind) = 1 + 2 * sum (A);
//   endif
// 
// endfunction
