// Copyright (C) 2012-2013 Holger Nahrstaedt
// Copyright (C) 1995-2012 Kurt Hornik
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [h, pval, chisq, df] = hypt_barttest (varargin)
//Bartlett's test for homogeneity of variances
//Calling Sequence
//[h, p, chisq, df] = hypt_barttest (x1,x2,...,xn)
//[h, p, chisq, df] = hypt_barttest (x1,x2,...,xn,alpha)
//Parameters
// x1, ..,xn: input vectors (could have different lengths)
// alpha: significance level (default alpha = 0.05)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p: p-value
// chisq: Bartlett's test statistic <latex>$X^2$</latex>
// df: degrees of freedom
// Description
// This function performs a Bartlett's test for the homogeneity of variances in the data
// vectors. There are k sample vectors <latex>$x_i$</latex>, where i=1..k. The length of  each sample vector is <latex>$n_i$</latex> and its sample variance is <latex>$S_i$</latex>. 
//
// Bartlett's test statistic is
//
// <latex>
// \begin{eqnarray}
// X^2 = \frac{(N-k)\ln(S_p^2) - \sum_{i=1}^k(n_i - 1)\ln(S_i^2)}{1 + \frac{1}{3(k-1)}\left(\sum_{i=1}^k(\frac{1}{n_i-1}) - \frac{1}{N-k}\right)}
// \end{eqnarray}
// </latex>
// 
// where 
// 
// <latex>$N = \sum_{i=1}^k n_i$</latex> 
// 
// and 
// 
// <latex>$S_p^2 = \frac{1}{N-k} \sum_i (n_i-1)S_i^2$</latex> 
// 
// is the pooled estimate for the variance.
// 
// Under the null of equal variances, the test statistic <latex>$X^2$</latex>
// approximately follows a chi-square distribution with k-1 degrees of
// freedom.
// 
// The null hypothesis is rejected if <latex>$X^2$</latex> is greater than the upper tail critical value for the chi square distribution with k-1 degrees of freedom.
//
// The p-value (1 minus the CDF of this distribution at <latex>$X^2$</latex>) is
// returned in p.
// Examples
// // Lets take a look on the PlantGrowth dataset from R
// // Ten plants are treated with 3 different treatments and its dried weight is 
// // saved in x1,x2 and x3:
// x1=[4.17 5.58 5.18 6.11   4.50  4.61   5.17   4.53  5.33  5.14 ];
// x2=[4.81  4.17   4.41  3.59  5.87    3.83  6.03   4.89   4.32    4.69];
// x3=[6.31   5.12   5.54  5.50   5.37  5.29  4.92  6.15   5.80   5.26];
// // We want to know if the variances of the weights are the same for all groups 
// // at a significance level of 0.05:
// [h, p, chisq, df] = hypt_barttest (x1,x2,x3,0.05)
// //As h is false and p is greater than 0.05, we cannot reject the null 
// //hypothesis. This means there is no evidence, that the variance is different 
// //in plant growth for the three treatment groups.
//
// Authors
// Copyright (C) 2012-2013 - Holger Nahrstaedt
// Copyright (C) 1995-2012 - Kurt Hornik



[nargout,nargin]=argn(0);

  k = nargin;
  if (k < 2)
   error("wrong number of parameters") ;
  end

  f = zeros (k, 1);
  v = zeros (k, 1);
  alpha=0.05;
  for i = 1 : k;
    x = varargin(i);
    if (~ isvector (x) & i<k)
      error ("bartlett_test: all arguments must be vectors");
    elseif (~ isvector (x) & i==k)
      alpha=x;
      k=k-1;
      f(k+1,1)=[];
      v(k+1,1)=[];
      break;
    end
    f(i) = length (x) - 1;
    v(i) = nanstdev (x).^2;
  end

  f_tot = sum (f);
  v_tot = sum (f .* v) / f_tot;
  c     = 1 + (sum (1 ./ f) - 1 / f_tot) / (3 * (k - 1));
  chisq = (f_tot * log (v_tot) - sum (f .* log (v))) / c;
  df    = k-1;
  pval  = 1 - distfun_chi2cdf (chisq, df);
  //pval  = 1 - distfun_gamcdf (chisq, df / 2, 2);
  //pval =1 -  gammainc (chisq ./ 2, df / 2);
  //pval =1 -  cdfgam("PQ",chisq ./ 2,df / 2,ones(df));

  h = pval < alpha;	


endfunction
