// Copyright (C) 2013 - Holger Nahrstaedt
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h,pval, z, ci] = hypt_ztest (x, m, sigma,alpha,tail,DIM)
//Test for mean of a normal sample with known variance
//Calling Sequence
//h = hypt_ztest (x, m, sigma)
//h = hypt_ztest (x, m, sigma,alpha)
//h = hypt_ztest (x, m, sigma,alpha,tail)
//h = hypt_ztest (x, m, sigma,alpha,tail,dim)
//[h,p] = hypt_ztest (...)
//[h,p, z] = hypt_ztest (...)
//[h,p, z, ci] = hypt_ztest (...)
// Parameters
// x: input vector
//  m: mean value for testing
//  sigma: known standard deviation of x
//  alpha: significance   level (default alpha = 0.05)
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative mean, which is the default setting ('~=', '<>', 'both') and the one-sided Alternatives ('>','right') and ('<', 'left').
// dim: if x is a matrix, dim defines the dimension along the test is performed (default is 'r' or 1)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
// ci: confidence internvals (depending on alpha)
// z: z statistic
// Description
// Perform a Z-test of the null hypothesis mean (x) == m for a sample x from a normal distribution with unknown
// mean and known variance sigma.  Under the null, the test statistic
// z follows a standard normal distribution.
//
//     With the optional argument string tail, the alternative of interest
//     can be selected.  If tail is '~=' or '<>' or 'both', the null hypothesis is tested
//     against the two-sided Alternative `mean (x) ~= mean (y)' or `mean (x) ~= m'.  
//     
//     If tail    is '>' or 'right', the one-sided Alternative `mean (x) > mean (y)' or `mean (x) > m' is used.
//     Similarly for '<' or 'left', the one-sided Alternative `mean (x) < mean (y)' or `mean (x) < m' is used.  The default is the two-sided case.
//Examples
// //10 volunteers performed an  intelligence test. 
// //We want to know if there is significant difference in the mean to mean of the 
// //entire population which is assumed to be 75.
// //Lets also assume that the sample variance is known and equal to 18
// x=[65, 78, 88, 55, 48, 95, 66, 57, 79, 81];
// [h,pval, z, ci] = hypt_ztest (x,75,sqrt(18),0.05)
// //We can reject the null hypothesis as h is true. 
// //The null hypothesis that the performance of the ten volunteers
// //are comparable to the entire population can be rejected with 
// //1-pval=99.5% confidence.
//
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt
// 


  [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_ztest" , rhs , 3:6 );
  apifun_checklhs ( "hypt_ztest" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_ztest" , x , "x" , 1 , "constant" );
  apifun_checkrange("hypt_ztest",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_ztest",max(size(x)),"minimal size of x",1,2 );
  apifun_checktype ( "hypt_ztest" , m , "m" , 2 , "constant" );
 apifun_checktype ( "hypt_ztest" , sigma , "sigma" , 3 , "constant" );
 

  if (rhs < 4) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 5) | isempty(tail)
    tail = '~=';
  end
  
  if (rhs < 6) | isempty(DIM)
    DIM = find(size(x)>1,1);
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
    
  elseif type(dim)~=10 then
    apifun_checktype ( "hypt_ztest" , DIM , "dim" , 5 , "constant" );
    apifun_checkrange("hypt_ztest",DIM,"dim",5,1,2 );
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
  end



  apifun_checktype ( "hypt_ztest" , alpha , "alpha" , 4 , "constant" );
  apifun_checktype ( "hypt_ztest" , tail , "tail" , 5 , "string" );
  apifun_checktype ( "hypt_ztest" , dim , "dim" , 6 , "string" );

 

  
    if (~ length (m)==1)
      error ("z_test: M must be a scalar");
    end
    if (~ (length (sigma)==1 & (sigma > 0)))
      error ("z_test: sigma must be a positive scalar");
    end


  N=size(x,dim);
  


    s = sigma ./ sqrt(N);
    z = (nanmean (x) - m)/s;
    cdf = erfc (z / (-sqrt(2))) / 2;


 

  if ( (tail== "~=") |  (tail == '!=') |  (tail== "<>")) | (tail == 'both'), 
    pval = 2 * min (cdf, 1 - cdf);
    crit = sqrt(2)*erfinv(2*(1 - alpha/2) - 1) .* s;
    ci = cat(1, m-crit, m+crit);
  elseif ( (tail== ">")) | (tail == 'right'),
    pval = 1 - cdf;
    crit = sqrt(2)*erfinv(2*(1 - alpha) - 1) .* s;
    ci = cat(1, m-crit, %inf);
  elseif ( (alt== "<")) | (tail == 'left'),
    pval = cdf;
    crit = sqrt(2)*erfinv(2*(1 - alpha) - 1) .* s;
    ci = cat(1, -%inf, m+crit);
  else
    error (sprintf("hypt_ztest: option %s not recognized\n", tail));
  end

  h = pval < alpha;

endfunction
