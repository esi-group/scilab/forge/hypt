// Copyright (C) 2013 - Holger Nahrstaedt
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h, pval, ci, stats] = hypt_vartest (x, sigma, alpha, tail,DIM)
// One-sample Chi-square  test of variance
// Calling Sequence
// h = hypt_vartest(x,sigma)
// h = hypt_vartest(x,sigma,alpha)
// h = hypt_vartest(x,sigma,alpha,tail)
// h = hypt_vartest(x,sigma,alpha,tail,dim)
// [h,p] = hypt_vartest(...)
// [h,p,ci] = hypt_vartest(...)
// [h,p,ci,stats] = hypt_vartest(...)
// Parameters
// x: input vector or matrix
// sigma: hypothesized standard deviation of the population (default sigma = 1)
// alpha: significance   level (default alpha = 0.05)
// tail: string, defines the alternative test hypothesis. Possible are the two-sided alternative ('~=', '<>', 'both') and the one-sided alternatives ('>','right') and ('<', 'left'). (default  tail = 'both')
// dim: if x is a matrix, dim defines the dimension along the test is performed (default is 'r' or 1)
// h: boolean, h=%t indicates a rejection of the null hypothesis and h=%f indicates a failure to reject the null hypothesis at the alpha (normally 5%) significance level.
// p:  p-value of the test
// ci: confidence internvals (depending on alpha)
// stats: structure with stats.tstat (value of test statistics), stats.df (degree of freedom), stats.sd (standard deviation estimate), stats.mean (mean estimate of x or of  the difference)
// Description
// A test of a single variance assumes that the underlying distribution is normal. The null and alternate hypotheses are stated in terms of the population variance (or population standard deviation).
// The test statistic is
//
// <latex>
// \begin{eqnarray}
// \frac{(n-1)s^2}{\sigma^2}
// \end{eqnarray}
// </latex>
// where <latex>$n$</latex> is the number of data, <latex>$s^2$</latex> is the sample variance and <latex>$\sigma^2$</latex> is the population variance.
// 
//     h=%t indicates a rejection of the Null-hypothesis at a significance 
//     level of alpha (default alpha = 0.05).	 
// Examples
// x = grand(20,1,"nor",15,7);
// [h,p,pi,stats]=hypt_vartest(x,6)
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt
// 


 [lhs,rhs]=argn();
  apifun_checkrhs ( "hypt_vartest" , rhs , 2:5 );
  apifun_checklhs ( "hypt_vartest" , lhs , 0:4 )
  //x must be a vector or a matrix
  apifun_checktype ( "hypt_vartest" , x , "x" , 1 , "constant" );
  apifun_checkrange("hypt_vartest",length(size(x)),"Dimension of x",1,1,2 );
  apifun_checkgreq("hypt_vartest",max(size(x)),"minimal size of x",1,2 );

 

  if (rhs < 3) | isempty(alpha)
    alpha = .05;
  end

  if (rhs < 4) | isempty(tail)
    tail = '~=';
  end
  
  if (rhs < 5) | isempty(DIM)
    DIM = find(size(x)>1,1);
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
    
  elseif type(dim)~=10 then
    apifun_checktype ( "hypt_vartest" , DIM , "dim" , 5 , "constant" );
    apifun_checkrange("hypt_vartest",DIM,"dim",5,1,2 );
    if (DIM==1) then
       dim='r';
    elseif (DIM==2) then
       dim='c';
    end;
  end


  apifun_checktype ( "hypt_vartest" , sigma , "sigma" , 2 , "constant" );
  apifun_checktype ( "hypt_vartest" , alpha , "alpha" , 3 , "constant" );
  apifun_checktype ( "hypt_vartest" , tail , "tail" , 4 , "string" );
  apifun_checktype ( "hypt_vartest" , dim , "dim" , 5 , "string" );

  
  szx = size(x); 
  szs = size(sigma);	
  //check nan


  [x1,x2]=find(isnan(x));
  N=size(x,dim)*ones(szs(1),szs(2));
  
  if (DIM==1) then
  for (i=1:size(x,2))
    N(i)=N(i)-length(find(x2==i));
  end;
  else
  for (i=1:size(x,1))
    N(i)=N(i)-length(find(x1==i));
  end;
  end;



  xmean=nanmean(x,dim);

  xvar=nanstdev(x,dim).^2;

  if (isvector(x)) then
    sumsq=sum((x - xmean).^2,dim);
  else
    sumsq=sum((x - repmat(xmean,N(DIM),1)).^2,dim);
  end;

  
  stats.df = N - 1;
  stats.chisqstat      = sumsq ./ (sigma.^2);
  stats.var = xvar;

    cdf = distfun_chi2cdf(stats.chisqstat, stats.df);
  
  if ( (tail== '~=') |  (tail == '~=') |  (tail == '<>')) | (tail == 'both'),
    pval = 2 * min (cdf, 1 - cdf);
    ci = cat(DIM, sumsq ./ distfun_chi2inv(1 - alpha/2, stats.df), sumsq ./ distfun_chi2inv(alpha/2, stats.df));
  elseif  (tail ==  '>') | (tail == 'right'),
    pval = 1 - cdf;
    ci = cat(DIM, sumsq ./ distfun_chi2inv(1 - alpha/2, stats.df), %inf);
  elseif  (tail == '<') | (tail == 'left'),
    pval = cdf;
    ci = cat(DIM, -%inf, sumsq ./ distfun_chi2inv(alpha/2, stats.df));
  else
    error (sprintf('hypt_vartest: option %s not recognized', tail));
  end

  h = (pval < alpha);

endfunction
