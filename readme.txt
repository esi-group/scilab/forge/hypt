Hypothesis testing : hypt

In this section, we present functions which compute tests, confidence intervals and model estimation.

One of the principles of this toolbox is to be compatible with MATLAB, especially the Hypothesis Testing sub-module:

http://www.mathworks.fr/fr/help/stats/hypothesis-tests-1.html

This section is a *DRAFT*.

High Priority List:

    hypt_barttest : Bartlett test for homogeneity of variances (http://en.wikipedia.org/wiki/Bartlett_test). MATLAB Compatible. Source of implementation : 
CACSCI ?
    hypt_crosstab Cross-tabulation. MATLAB Compatible. Source of implementation : TODO ?

    hypt_chi2gof : CHI Squared Goodness Of Fit test (http://en.wikipedia.org/wiki/Pearson%27s_chi-squared_test). MATLAB Compatible. Source of implementation : 
TODO ?

    hypt_kstest: Perform a one sample Kolmogorov-Smirnov test of the null hypothesis that the samples x comes from given distribution 
(http://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test). MATLAB Compatible. Source of implementation : TODO ?

    hypt_kstest2 : Kolmogorov-Smirnov statistic from two samples (http://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test). MATLAB Compatible. Source of 
implementation : kstwo in Stixbox ?
    hypt_ranksum Wilcoxon rank sum test. MATLAB Compatible. Source of implementation : TODO ?

    hypt_signrank Wilcoxon signed rank test (http://en.wikipedia.org/wiki/Wilcoxon_signed-rank_test). MATLAB Compatible. Source of implementation : TODO ?

    hypt_signtest Sign test (http://en.wikipedia.org/wiki/Sign_test). MATLAB Compatible. Source of implementation : TODO ?

    hypt_ttest One-sample and paired-sample t-test (http://en.wikipedia.org/wiki/Student%27s_t-test). MATLAB Compatible. Source of implementation : TODO ?

    hypt_ttest2 Two-sample t-test (http://en.wikipedia.org/wiki/Student%27s_t-test). MATLAB Compatible. Source of implementation : TODO ?

    hypt_vartest Chi-square variance test (http://en.wikipedia.org/wiki/Chi-squared_test). MATLAB Compatible. Source of implementation : TODO ?

    hypt_vartest2 Two-sample F-test for equal variances (http://en.wikipedia.org/wiki/F-test_of_equality_of_variances). MATLAB Compatible. Source of 
implementation : TODO ?

    zhypt_ztest z-test (Test for mean of a normal sample with known variance) (http://en.wikipedia.org/wiki/Z-test). MATLAB Compatible. Source of implementation 
: TODO ? 

Low Priority List:

    hypt_ciquant : Nonparametric confidence interval for quantile
    hypt_cmpmod : Compare linear submodel versus larger one
    hypt_test1b : Bootstrap t test and confidence interval for the mean
    hypt_test1n : Tests and confidence intervals based on a normal sample
    hypt_test1r : Test for median equals 0 using rank test
    hypt_test2n : Tests and confidence intervals based on two normal samples with common variance

    hypt_welchtest : Welch two-sample t test (http://en.wikipedia.org/wiki/Welch%27s_t_test)

    hypt_kriskalwallis : Perform a Kruskal-Wallis one-factor "analysis of variance". (http://en.wikipedia.org/wiki/Kruskal-Wallis)

    hypt_utest (the same as test2r) : Perform a Mann-Whitney U-test(http://en.wikipedia.org/wiki/Mann-Whitney-Wilcoxon_test)

    hypt_mcnemartest : McNemar's test for symmetry (http://en.wikipedia.org/wiki/McNemar%27s_test) 

Excluded The following functions will not be part of the hypt toolbox:

    lsfit : Fit a multiple regression normal model. This function is more appropriate in the "regression" toolbox.
    lsselect : Select a predictor subset for regression. This function is more appropriate in the "regression" toolbox. 


TODO

Design:

    Rename the function to match Matlab
    Insert a prefix to avoid name conflicts
    Add missing functions
    Indicate potential implementations 

Development:

    write unit tests (As reference R could be used)
    create demos
    create help pages for each function 

Authors

    2012-2013, Michaël Baudin
    2012, Holger Nahrstaedt 
