mode(1);
// We have a treatment and a measurement on 10 subjects before the treatment and after the treatment
// Now we testing if the treatment does has any effect:
//
before=[223, 259, 248, 220, 287, 191, 229, 270, 245, 201];
after =[220, 244, 243, 211, 299, 170, 210, 276, 252, 189];
//
[h,p]=hypt_ttest(before,after);
//
// h=0 means that the treatment does not have any effect or the number is to small.
// h=1 would mean that treatment has either a good or bad effect
mode(-1);
printf("h=%d\n",h);
mode(1);

// The treatment does not have any effect or the number is to small 
  