//library(TeachingDemos)
// library(MASS)
//x <- PlantGrowth$weight

x = [4.17 5.58 5.18 6.11 4.50 4.61 5.17 4.53 5.33 5.14 4.81 4.17 4.41 3.59 5.87,..
 3.83 6.03 4.89 4.32 4.69 6.31 5.12 5.54 5.50 5.37 5.29 4.92 6.15 5.80 5.26
];



 
 
 //txy <- sigma.test(x, sigma=1)
 //print(txy$statistic,digits=20)
 x_squared=14.258430000000000604;
 //print(txy$parameter,digits=17)
 df_test=29
 //print(txy$p.value,digits=20)
 p_xy_pair=0.020022484614741518222;

 //print(txy$estimate,digits=20)
 ratio_var_xy_pair = [0.49166999999999999593];
 
 //print(txy$conf.int[1:2],digits=20)
 ci_xy_pair =[0.31184858213493288348 0.88853781366967232724];
 //alternative hypothesis: true variance is not equal to 1
 h_test=%t
 
 [h,p,ci,stats] = hypt_vartest(x,1,0.05,'both');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( [stats.df] , df_test ,  [], 1e-17 );
 assert_checkalmostequal ( stats.chisqstat , x_squared ,  [], 1e-17 );
 assert_checktrue(h==h_test);
 
  [h,p,ci,stats] = hypt_vartest(x',1,0.05,'both');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair' ,  [], 1e-17 );
 assert_checkalmostequal (  [stats.df] , df_test ,  [], 1e-17 );
 assert_checkalmostequal ( stats.chisqstat , x_squared ,  [], 1e-17 );
 assert_checktrue(h==h_test);
 
 
  [h,p,ci,stats] = hypt_vartest([x',x'],[1,1],0.05,'both');
 
 assert_checkalmostequal ( p , [p_xy_pair',p_xy_pair'] , [], 1e-17);
 assert_checkalmostequal ( ci , [ci_xy_pair',ci_xy_pair'] ,  [], 1e-17 );
 assert_checkalmostequal (  [stats.df] , [df_test,df_test] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.chisqstat , [x_squared,x_squared] ,  [], 1e-17 );
 assert_checktrue(and(h==h_test));
 
 
 