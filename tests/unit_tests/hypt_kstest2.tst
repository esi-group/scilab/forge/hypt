// library(MASS)
//x <- PlantGrowth$weight

x = [4.17 5.58 5.18 6.11 4.50 4.61 5.17 4.53 5.33 5.14 4.81 4.17 4.41 3.59 5.87,..
 3.83 6.03 4.89 4.32 4.69 6.31 5.12 5.54 5.50 5.37 5.29 4.92 6.15 5.80 5.26
];
// y<-x-mean(x)

y=x-mean(x);

 //txy <- ks.test(x,y,"pnorm")

 //print(txy$p.value,digits=20)
 p_x_onesample=1.8718360195180139272e-13;

 
 //alternative hypothesis: true mean is not equal to 18 
 h_test=%t;
 
 [h,p] = hypt_kstest2(x,y);
 assert_checkalmostequal ( p , p_x_onesample , [], 1e-17);
 assert_checktrue(h==h_test);
 
