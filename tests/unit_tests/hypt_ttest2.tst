//library(MASS)
//x <- survey$Wr.Hnd

x = [18.5 19.5 18.0 18.8 20.0 18.0 17.7 17.0 20.0 18.5 17.0 21.0 16.0 19.5 16.0,..
    17.5 18.0 19.4 20.5 21.0 21.5 20.1 18.5 21.5 17.0 18.5 21.0 20.8 17.8 19.5,..
   18.5 18.8 17.1 20.1 18.0 22.2 16.0 19.4 22.0 19.0 17.5 17.8   %nan 20.1 13.0,..
  17.0 23.2 22.5 18.0 18.0 22.0 20.5 17.0 20.5 22.5 18.5 15.5 19.5 19.5 20.6,..
  22.8 18.5 19.6 18.7 17.3 19.5 19.0 18.5 19.0 21.0 18.0 19.4 17.0 16.5 15.6,..
  17.5 17.0 18.6 18.3 20.0 19.5 19.2 17.5 17.0 23.0 17.7 18.2 18.3 18.0 18.0,..
  20.5 17.5 18.2 18.2 21.3 19.0 20.0 17.5 19.5 19.4 21.9 18.9 16.0 17.5 17.5,..
 19.5 16.2 17.0 17.5 19.7 18.5 19.2 17.2 20.5 16.0 16.9 17.0 23.0 18.5 21.0,..
 20.0 22.5 18.5 19.8 18.5 19.3 16.0 18.8 17.5 16.4 22.0 19.0 18.9 15.4 17.9,..
 23.1 19.8 22.0 20.0 19.5 18.0 18.3 19.0 21.4 20.0 18.5 22.5 19.5 18.0 18.0,..
 21.8 13.0 16.3 21.5 18.9 20.5 14.0 18.9 20.0 18.5 17.5 18.1 20.2 16.5 19.1,..
 17.6 19.5 16.5 19.0 19.0 16.5 20.5 15.5 18.0 17.5 19.0 20.5 16.7 20.5 17.0,..
 19.0 14.0 17.5 18.5 18.0 20.5 17.0 18.5 18.0 18.5 20.0 22.0 17.9 17.6 16.7,..
 17.0 15.0 16.0 19.1 17.5 16.2 21.0 18.8 18.5 17.0 17.5 17.5 17.5 17.5 20.8,..
 18.6 17.5 18.0 17.0 18.0 19.5 16.3 18.2 17.0 23.2 23.2 15.9 17.5 17.5 17.6,..
 17.5 18.8 20.0 18.6 18.6 18.8 18.0 18.0 18.5 17.5 21.0 17.6];

 //y <- survey$NW.Hnd
 y=[18.0 20.5 13.3 18.9 20.0 17.7 17.7 17.3 19.5 18.5 17.2 21.0 16.0 20.2 15.5,..
 17.0 18.0 19.2 20.5 20.9 22.0 20.7 18.0 21.2 17.5 18.5 20.7 21.4 17.8 19.5,..
 18.0 18.2 17.5 20.0 19.0 21.0 16.5 18.5 22.0 19.0 16.0 18.0   %nan 20.2 13.0,..
 17.5 22.7 23.0 17.6 17.9 21.5 20.0 18.0 19.5 22.5 18.5 15.4 19.7 19.0 21.0,..
 23.2 18.2 19.7 18.0 18.0 19.8 19.1 18.0 19.0 19.5 17.5 19.5 16.6 17.0 15.8,..
 17.5 17.6 18.0 18.5 20.5 19.5 18.9 17.5 17.4 23.5 17.0 18.0 18.5 18.0 17.7,..
 20.0 18.0 17.5 18.5 20.8 18.8 19.5 17.5 19.4 19.6 22.2 19.1 16.0 17.3 17.0,..
 18.5 16.4 15.9 17.5 20.1 18.5 19.6 16.7 21.0 15.5 16.0 16.7 22.0 18.0 20.4,..
 20.0 22.5 18.0 20.0 18.1 19.4 16.0 19.1 17.0 16.5 21.5 19.5 20.0 16.4 17.8,..
 22.5 19.0 22.0 19.5 18.5 18.6 19.0 18.8 21.0 19.5 18.5 22.6 20.2 18.0 18.5,..
 22.3 12.5 16.2 21.6 19.1 20.0 15.5 19.2 20.5 19.0 17.1 18.2 20.3 16.9 19.1,..
 17.2 19.2 15.0 18.5 18.5 17.0 19.5 15.5 17.5 18.0 18.5 20.5 17.0 20.5 16.5,..
 19.5 13.5 17.6 19.0 18.5 20.7 17.0 18.5 18.5 18.0 19.5 22.5 18.4 17.8 15.1,..
 17.6 13.0 15.5 19.0 16.5 15.8 21.0 17.8 18.0 17.5 17.0 17.6 17.6 17.0 20.7,..
 18.6 17.5 18.5 17.5 17.8 20.0 16.2 19.8 17.3 23.2 23.3 16.5 18.4 17.6 17.2,..
 17.8 18.3 19.8 18.8 19.6 18.5 16.0 18.0 18.0 16.5 21.5 17.3
];



 
 
 //txy <- t.test(x, y, var.equal=TRUE)
 //print(txy$statistic,digits=20)
 txy_pair=0.48815802485157927526;
 //print(txy$parameter,digits=17)
 df_xy_pair=470;
 //print(txy$p.value,digits=20)
 p_xy_pair=0.6256655703604157015;

 //print(txy$estimate,digits=20)
 xmean_xy_pair = [18.669067796610168131 18.582627118644069242 ];
 
 //print(txy$conf.int[1:2],digits=20)
 ci_xy_pair =[-0.26151636259232396986  0.43439771852452174761];
 
 //alternative hypothesis: true difference in means is not equal to 0 
 h_test=%f
 
 
 [h,p,ci,stats] = hypt_ttest2(x,y,0.05,'both','equal');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( [stats.xmean,stats.ymean] , xmean_xy_pair ,  [], 1e-17);
  assert_checktrue(h==h_test);
 
  [h,p,ci,stats] = hypt_ttest2(x',y',0.05,'both','equal');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair' ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal (  [stats.xmean,stats.ymean] , xmean_xy_pair ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
 
  [h,p,ci,stats] = hypt_ttest2([x',x'],[y',y'],0.05,'both','equal');
 
 assert_checkalmostequal ( p , [p_xy_pair',p_xy_pair'] , [], 1e-17);
 assert_checkalmostequal ( ci , [ci_xy_pair',ci_xy_pair'] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , [df_xy_pair,df_xy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , [txy_pair,txy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( [stats.xmean;stats.ymean] , [xmean_xy_pair',xmean_xy_pair'] ,  [], 1e-17);
 assert_checktrue(and(h==h_test));
 
 
  
 //txy <- t.test(x, y, var.equal=FALSE)
 //print(txy$statistic,digits=20)
 txy_pair=0.48815802485157921975 ;
 //print(txy$parameter,digits=20)
 df_xy_pair=469.01693316999632088;
  //print(txy$p.value,digits=20)
 p_xy_pair=0.62566604737607300901;

 //print(txy$estimate,digits=20)
 xmean_xy_pair = [ 18.669067796610168131 18.582627118644069242 ];
 
 //print(txy$conf.int[1:2],digits=20)
 ci_xy_pair =[-0.26151824546359186963  0.43439960139578959186];
 //alternative hypothesis: true difference in means is not equal to 0 
 h_test=%f;
 
 
  [h,p,ci,stats] = hypt_ttest2(x,y,0.05,'both','unequal');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( [stats.xmean,stats.ymean] , xmean_xy_pair ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
  [h,p,ci,stats] = hypt_ttest2(x',y',0.05,'both','unequal');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair' ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal (  [stats.xmean,stats.ymean] , xmean_xy_pair ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
 
  [h,p,ci,stats] = hypt_ttest2([x',x'],[y',y'],0.05,'both','unequal');
 
 assert_checkalmostequal ( p , [p_xy_pair',p_xy_pair'] , [], 1e-17);
 assert_checkalmostequal ( ci , [ci_xy_pair',ci_xy_pair'] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , [df_xy_pair,df_xy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , [txy_pair,txy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( [stats.xmean;stats.ymean] , [xmean_xy_pair',xmean_xy_pair'] ,  [], 1e-17);
 assert_checktrue(and(h==h_test));