//library(MASS)
//x <- survey$Wr.Hnd

x = [18.5 19.5 18.0 18.8 20.0 18.0 17.7 17.0 20.0 18.5 17.0 21.0 16.0 19.5 16.0,..
    17.5 18.0 19.4 20.5 21.0 21.5 20.1 18.5 21.5 17.0 18.5 21.0 20.8 17.8 19.5,..
   18.5 18.8 17.1 20.1 18.0 22.2 16.0 19.4 22.0 19.0 17.5 17.8   %nan 20.1 13.0,..
  17.0 23.2 22.5 18.0 18.0 22.0 20.5 17.0 20.5 22.5 18.5 15.5 19.5 19.5 20.6,..
  22.8 18.5 19.6 18.7 17.3 19.5 19.0 18.5 19.0 21.0 18.0 19.4 17.0 16.5 15.6,..
  17.5 17.0 18.6 18.3 20.0 19.5 19.2 17.5 17.0 23.0 17.7 18.2 18.3 18.0 18.0,..
  20.5 17.5 18.2 18.2 21.3 19.0 20.0 17.5 19.5 19.4 21.9 18.9 16.0 17.5 17.5,..
 19.5 16.2 17.0 17.5 19.7 18.5 19.2 17.2 20.5 16.0 16.9 17.0 23.0 18.5 21.0,..
 20.0 22.5 18.5 19.8 18.5 19.3 16.0 18.8 17.5 16.4 22.0 19.0 18.9 15.4 17.9,..
 23.1 19.8 22.0 20.0 19.5 18.0 18.3 19.0 21.4 20.0 18.5 22.5 19.5 18.0 18.0,..
 21.8 13.0 16.3 21.5 18.9 20.5 14.0 18.9 20.0 18.5 17.5 18.1 20.2 16.5 19.1,..
 17.6 19.5 16.5 19.0 19.0 16.5 20.5 15.5 18.0 17.5 19.0 20.5 16.7 20.5 17.0,..
 19.0 14.0 17.5 18.5 18.0 20.5 17.0 18.5 18.0 18.5 20.0 22.0 17.9 17.6 16.7,..
 17.0 15.0 16.0 19.1 17.5 16.2 21.0 18.8 18.5 17.0 17.5 17.5 17.5 17.5 20.8,..
 18.6 17.5 18.0 17.0 18.0 19.5 16.3 18.2 17.0 23.2 23.2 15.9 17.5 17.5 17.6,..
 17.5 18.8 20.0 18.6 18.6 18.8 18.0 18.0 18.5 17.5 21.0 17.6];

 //y <- survey$Height
 y= [173.00 177.80     %nan 160.00 165.00 172.72 182.88 157.00 175.00 167.00,..
 156.20     %nan 155.00 155.00     %nan 156.00 157.00 182.88 190.50 177.00,..
190.50 180.34 180.34 184.00     %nan     %nan 172.72 175.26     %nan 167.00,..
     %nan 180.00 166.40 180.00     %nan 190.00 168.00 182.50 185.00 171.00,..
 169.00 154.94 172.00 176.50 180.34 180.34 180.00 170.00 168.00 165.00,..
 200.00 190.00 170.18 179.00 182.00 171.00 157.48     %nan 177.80 175.26,..
 187.00 167.64 178.00 170.00 164.00 183.00 172.00     %nan 180.00     %nan,..
 170.00 176.00 171.00 167.64 165.00 170.00 165.00 165.10 165.10 185.42,..
     %nan 176.50     %nan     %nan 167.64 167.00 162.56 170.00 179.00     %nan,..
 183.00     %nan 165.00 168.00 179.00     %nan 190.00 166.50 165.00 175.26,..
 187.00 170.00 159.00 175.00 163.00 170.00 172.00     %nan 180.00 180.34,..
 175.00 190.50 170.18 185.00 162.56 158.00 159.00 193.04 171.00 184.00,..
     %nan 177.00 172.00 180.00 175.26 180.34 172.72 178.50 157.00 152.00,..
 187.96 178.00     %nan 160.02 175.26 189.00 172.00 182.88 170.00 167.00,..
 175.00 165.00 172.72 180.00 172.00 185.00 187.96 185.42 165.00 164.00,..
 195.00 165.00 152.40 172.72 180.34 173.00     %nan 167.64 187.96 187.00,..
 167.00 168.00 191.80 169.20 177.00 168.00 170.00 160.02 189.00 180.34,..
 168.00 182.88     %nan 165.00 157.48 170.00 172.72 164.00     %nan 162.56,..
 172.00 165.10 162.50 170.00 175.00 168.00 163.00 165.00 173.00 196.00,..
 179.10 180.00 176.00 160.02 157.48 165.00 170.18 154.94 170.00 164.00,..
 167.00 174.00     %nan 160.00 179.10 168.00 153.50 160.00 165.00 171.50,..
 160.00 163.00     %nan 165.00 168.90 170.00     %nan 185.00 173.00 188.00,..
 171.00 167.64 162.56 150.00     %nan     %nan 170.18 185.00 167.00 185.00,..
 169.00 180.34 165.10 160.00 170.00 183.00 168.50];


 //txy <- t.test(x, mu = 18)
 //print(txy$statistic,digits=20)
 tx_onesample=5.4702056169010679909;
 //print(txy$parameter,digits=17)
 df_x_onesample=235;
 //print(txy$p.value,digits=20)
 p_x_onesample=1.1482028256688668214e-07;

 //print(txy$estimate,digits=20)
 xmean_x_onesample = 18.669067796610168131
 
 //print(txy$conf.int[1:2],digits=20)
 ci_x_onesample =[18.428101132693797126 18.910034460526539135];
 //alternative hypothesis: true mean is not equal to 18 
 h_test=%t
 
 [h,p,ci,stats] = hypt_ttest(x,18,0.05,'both');
 
 assert_checkalmostequal ( p , p_x_onesample , [], 1e-17);
 assert_checkalmostequal ( ci , ci_x_onesample ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_x_onesample ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , tx_onesample ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , xmean_x_onesample ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
 [h,p,ci,stats] = hypt_ttest(x',18,0.05,'both');
 
 assert_checkalmostequal ( p , p_x_onesample' , [], 1e-17);
 assert_checkalmostequal ( ci , ci_x_onesample' ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , tx_onesample' ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , xmean_x_onesample' ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
 //txy <- t.test(x, y, paired=TRUE)
 //print(txy$statistic,digits=20)
 txy_pair=-250.11729105569727949;
 //print(txy$parameter,digits=17)
 df_xy_pair=207;
 //print(txy$p.value,digits=20)
 p_xy_pair=7.6354521695113046807e-259;

 //print(txy$estimate,digits=20)
 xmean_xy_pair = -153.63509615384614904
 
 //print(txy$conf.int[1:2],digits=20)
 ci_xy_pair =[-154.84608846326418075 -152.42410384442811733];
 //alternative hypothesis: true difference in means is not equal to 0 
 h_test=%t
 
 
 [h,p,ci,stats] = hypt_ttest(x,y,0.05,'both');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , xmean_xy_pair ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
  [h,p,ci,stats] = hypt_ttest(x',y',0.05,'both');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair' ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , xmean_xy_pair ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
 
  [h,p,ci,stats] = hypt_ttest([x',x'],[y',y'],0.05,'both');
 
 assert_checkalmostequal ( p , [p_xy_pair',p_xy_pair'] , [], 1e-17);
 assert_checkalmostequal ( ci , [ci_xy_pair',ci_xy_pair'] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , [df_xy_pair,df_xy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , [txy_pair,txy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , [xmean_xy_pair,xmean_xy_pair] ,  [], 1e-17);
 assert_checktrue(and(h==h_test));
 
 
 //y <- survey$NW.Hnd
 y=[18.0 20.5 13.3 18.9 20.0 17.7 17.7 17.3 19.5 18.5 17.2 21.0 16.0 20.2 15.5,..
  17.0 18.0 19.2 20.5 20.9 22.0 20.7 18.0 21.2 17.5 18.5 20.7 21.4 17.8 19.5,..
  18.0 18.2 17.5 20.0 19.0 21.0 16.5 18.5 22.0 19.0 16.0 18.0   %nan 20.2 13.0,..
  17.5 22.7 23.0 17.6 17.9 21.5 20.0 18.0 19.5 22.5 18.5 15.4 19.7 19.0 21.0,..
 23.2 18.2 19.7 18.0 18.0 19.8 19.1 18.0 19.0 19.5 17.5 19.5 16.6 17.0 15.8,..
 17.5 17.6 18.0 18.5 20.5 19.5 18.9 17.5 17.4 23.5 17.0 18.0 18.5 18.0 17.7,..
 20.0 18.0 17.5 18.5 20.8 18.8 19.5 17.5 19.4 19.6 22.2 19.1 16.0 17.3 17.0,..
 18.5 16.4 15.9 17.5 20.1 18.5 19.6 16.7 21.0 15.5 16.0 16.7 22.0 18.0 20.4,..
 20.0 22.5 18.0 20.0 18.1 19.4 16.0 19.1 17.0 16.5 21.5 19.5 20.0 16.4 17.8,..
 22.5 19.0 22.0 19.5 18.5 18.6 19.0 18.8 21.0 19.5 18.5 22.6 20.2 18.0 18.5,..
 22.3 12.5 16.2 21.6 19.1 20.0 15.5 19.2 20.5 19.0 17.1 18.2 20.3 16.9 19.1,..
 17.2 19.2 15.0 18.5 18.5 17.0 19.5 15.5 17.5 18.0 18.5 20.5 17.0 20.5 16.5,..
 19.5 13.5 17.6 19.0 18.5 20.7 17.0 18.5 18.5 18.0 19.5 22.5 18.4 17.8 15.1,..
 17.6 13.0 15.5 19.0 16.5 15.8 21.0 17.8 18.0 17.5 17.0 17.6 17.6 17.0 20.7,..
 18.6 17.5 18.5 17.5 17.8 20.0 16.2 19.8 17.3 23.2 23.3 16.5 18.4 17.6 17.2,..
 17.8 18.3 19.8 18.8 19.6 18.5 16.0 18.0 18.0 16.5 21.5 17.3];
 
 
  //txy <- t.test(x, y, paired=TRUE)
 //print(txy$statistic,digits=20)
 txy_pair=2.1267722630439811482;
 //print(txy$parameter,digits=17)
 df_xy_pair=235;
 //print(txy$p.value,digits=20)
 p_xy_pair=0.034481384437298624734;

 //print(txy$estimate,digits=20)
 xmean_xy_pair = 0.086440677966101692187
 
 //print(txy$conf.int[1:2],digits=20)
 ci_xy_pair =[0.0063673892508576638433, 0.1665139666813457153260];
 //alternative hypothesis: true difference in means is not equal to 0 
 h_test=%t
 
 
 [h,p,ci,stats] = hypt_ttest(x,y,0.05,'both');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , xmean_xy_pair ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
  [h,p,ci,stats] = hypt_ttest(x',y',0.05,'both');
 
 assert_checkalmostequal ( p , p_xy_pair , [], 1e-17);
 assert_checkalmostequal ( ci , ci_xy_pair' ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , df_xy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , txy_pair ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , xmean_xy_pair ,  [], 1e-17);
 assert_checktrue(h==h_test);
 
 
  [h,p,ci,stats] = hypt_ttest([x',x'],[y',y'],0.05,'both');
 
 assert_checkalmostequal ( p , [p_xy_pair',p_xy_pair'] , [], 1e-17);
 assert_checkalmostequal ( ci , [ci_xy_pair',ci_xy_pair'] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.df , [df_xy_pair,df_xy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.tstat , [txy_pair,txy_pair] ,  [], 1e-17 );
 assert_checkalmostequal ( stats.mean , [xmean_xy_pair,xmean_xy_pair] ,  [], 1e-17);
 assert_checktrue(and(h==h_test));
