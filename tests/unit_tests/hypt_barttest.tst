//require(graphics)
//x <- InsectSprays$count
//
x=[10  7 20 14 14 12 10 23 17 20 14 13 11 17 21 11 16 14 17 17 19 21  7 13  0,..                                                                                                                  
  1  7  2  3  1  2  1  3  0  1  4  3  5 12  6  4  3  5  5  5  5  2  4  3  5,..
  3  5  3  6  1  1  3  2  6  4 11  9 15 22 15 16 13 10 26 26 24 13];
  
//y <-InsectSprays$spray
y =["A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "B", "B", "B", "B", "B", "B", "B", "B", "B", "B", "B", "B", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "D", "D",..
 "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F"];

  ident=unique(y)
  
  xA = x(find(y==ident(1)));
  xB = x(find(y==ident(2)));
  xC = x(find(y==ident(3)));
  xD = x(find(y==ident(4)));
  xE = x(find(y==ident(5)));
  xF = x(find(y==ident(6)));
 
 
//tst <- bartlett.test(InsectSprays$count, InsectSprays$spray)
//print(tst$statistic,digits=20)
Ksquared=25.959825320368686619  ;
//print(tst$p.value,digits=20)

p_value = 9.0851223329453144393e-05;

// print(tst$parameter,digits=20)
df_value = 5

  
[h, pval, chisq, df] = hypt_barttest(xA,xB,xC,xD,xE,xF);
  
assert_checkalmostequal ( chisq , Ksquared , [], 1e-17);
  
assert_checkalmostequal ( df , df_value , [], 1e-17);

 assert_checkalmostequal ( pval , p_value , [], 1e-17); 
  
//bartlett.test(count ~ spray, data = InsectSprays)